﻿using API.Model;
using API.Model.DataTypesForControllers;
using API.Model.Vehicles;
using API.Services.ServicesAbstraction;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Route("api/[controller]")]
    public class CarController : ControllerBase
    {
        ICarService _carService;

        public CarController(ICarService carService)
        {
            _carService = carService;
        }

        [HttpPost("add")]
        public ActionResult<string> AddCar([FromBody]ControllerCar car)
        {
            return _carService.AddCar(car);
        }

        [HttpDelete("remove/{id}")]
        public ActionResult<string> RemoveCar(int id)
        {
            return _carService.RemoveCar(id);
        }

        [HttpGet("rates")]
        public IEnumerable<string> GetRates()
        {
            return _carService.GetRates();
        }

        [HttpGet("getAll")]
        public IEnumerable<Vehicle> GetAll()
        {
            return _carService.GetAllCars();
        }

        [HttpGet("getOnParking")]
        public IEnumerable<Vehicle> GetAllOnParking()
        {
            return _carService.GetAllVehiclesOnParking();
        }

        [HttpPost("replenishBalance")]
        public ActionResult<string> ReplenishBalance([FromBody]AddCarBalance carBalance)
        {
            if (carBalance.Balance <= 0 || carBalance.CarId <= 0)
                return "Fill in all data";
            return _carService.ReplenishBalance(carBalance.CarId, carBalance.Balance);
        }

    }
}
