﻿using API.Model;
using API.Services.ServicesAbstraction;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Route("api/[controller]")]
    public class ParkingController : ControllerBase
    {
        IParkingService _parkingService;
        ICarService _carService;

        public ParkingController(ICarService carService, IParkingService parkingService)
        {
            _parkingService = parkingService;
            _carService = carService;
        }
        
        [HttpPut("putToParking/{id}")]
        public ActionResult<string> PutToParking(int id)
        {
            return _parkingService.PutToParking(id);
        }

        [HttpPut("removeFromParking/{id}")]
        public ActionResult<string> RemoveFromParking(int id)
        {
            return _parkingService.RemoveFromParking(id);
        }

        [HttpGet("allTransactions")]
        public IEnumerable<Transaction> GetAllTransactions(int id)
        {
            return _parkingService.GetAllTransactions();
        }

        [HttpGet("lastTransactions")]
        public IEnumerable<Transaction> GetLastTransactions(int id)
        {
            return _parkingService.GetLastTransactions();
        }

        [HttpGet("getBalance")]
        public ActionResult<string> GetBalance()
        {
            return _parkingService.GetBalance();
        }

        [HttpGet("places")]
        public IEnumerable<int> GetPlacesCount()
        {
            return _parkingService.GetPlaces();
        }
    }
}
