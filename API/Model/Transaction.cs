﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Model
{
    public class Transaction
    {
        public DateTime Time { get; private set; }
        public int VehicleId { get; private set; }
        public double Summ { get; private set; }

        public Transaction(int vehicleId, double summ, DateTime time)
        {
            Time = time;
            VehicleId = vehicleId;
            Summ = summ;
        }
    }
}
