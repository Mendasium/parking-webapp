﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Model.DataTypesForControllers
{
    public class ControllerCar
    {
        public int Id { get; set; }
        public double Balance { get; set; }
        public string Type { get; set; }
    }
}
