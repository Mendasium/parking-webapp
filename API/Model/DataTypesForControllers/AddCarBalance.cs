﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Model.DataTypesForControllers
{
    public class AddCarBalance
    {
        public int CarId { get; set; }
        public double Balance { get; set; }
    }
}
