﻿using System;

namespace API.Model
{
    public class StayedVehicle
    {
        public int vehicleId;
        public DateTime time;
        public double summ;
        public bool FineStarted { get; private set; } = false;
        
        public void StartFine()
        {
            FineStarted = true;
        }
    }
}
