﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Model.Vehicles
{
    public class Truck : Vehicle
    {
        public override double Rate => Settings.Rate["Truck"];
    }
}
