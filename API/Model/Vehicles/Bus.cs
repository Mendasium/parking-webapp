﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Model.Vehicles
{
    public class Bus : Vehicle
    {
        public override double Rate => Settings.Rate["Bus"];
    }
}
