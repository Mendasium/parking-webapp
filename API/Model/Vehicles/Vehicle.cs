﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace API.Model.Vehicles
{
    public abstract class Vehicle
    {
        public int id;
        public double Balance { get; private set; }
        public abstract double Rate { get; }

        public override string ToString()
        {
            return id.ToString();
        }

        public void AddBalance(double money)
        {
            if(money > 0)
                Balance += money;
        }

        public void RemoveBalance(double money)
        {
            if (money < Balance)
                Balance -= money;
            else
                throw new Exception($"Auto {id} doen't has enough money({Balance}) for decreasing {money}");
        }
    }
}
