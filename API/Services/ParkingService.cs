﻿using API.Model;
using API.Model.Vehicles;
using API.Services.ServicesAbstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Services
{
    public class ParkingService : IParkingService
    {

        public IEnumerable<Transaction> GetAllTransactions()
        {
            return Parking.GetInstance().ShowAllTransation();
        }

        public IEnumerable<Transaction> GetLastTransactions()
        {
            return Parking.GetInstance().ShowTransactionByMinute();
        }

        public string PutToParking(int id)
        {
            Vehicle car = Parking.GetInstance().GetCarById(id);
            if (car == null)
            {
                return "Such vehicle does not exist";
            }
            if (Parking.GetInstance().GetEmptyPlacesCount() <= 0)
            {
                return "All parking lots are busy";
            }
            return Parking.GetInstance().PutToParking(new StayedVehicle
            {
                vehicleId = car.id,
                time = DateTime.Now
            });
        }

        public string RemoveFromParking(int id)
        {
            Vehicle car = Parking.GetInstance().GetCarById(id);
            if (car == null)
            {
                return "Such vehicle doesn't exist";
            }
            return Parking.GetInstance().RemoveFromParking(id);
        }

        public string GetBalance()
        {
            return Parking.GetInstance().Balance.ToString();
        }

        public IEnumerable<int> GetPlaces()
        {
            int[] places = new int[3];
            places[0] = Settings.VehicleMaxCount;
            places[1] = Parking.GetInstance().GetEmptyPlacesCount();
            places[2] = Parking.GetInstance().GetBusyPlacesCount();
            return places;
        }
    }
}
