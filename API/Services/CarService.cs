﻿using API.Model;
using API.Model.DataTypesForControllers;
using API.Model.Vehicles;
using API.Services.ServicesAbstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Services
{
    public class CarService : ICarService
    {
        public Vehicle GetCarById(int id)
        {
            return Parking.GetInstance().GetCarById(id);
        }

        public string AddCar(ControllerCar vehicle)
        {
            if (Parking.GetInstance().GetCarById(vehicle.Id) != null)
            {
                return "Such vehicle is already in the parking lot";
            }
            Vehicle car;
            try
            {
                car = (Vehicle)Activator.CreateInstance(Type.GetType("API.Model.Vehicles." + vehicle.Type));
            }
            catch (ArgumentNullException)
            {
                return "Incorrect vehicle type";
            }
            car.AddBalance(vehicle.Balance);
            return Parking.GetInstance().AddVehicle(car);
        }

        public string RemoveCar(int id)
        {
            if (Parking.GetInstance().GetCarById(id) == null)
            {
                return "Such vehicle isn't in the parking lot";
            }
            return Parking.GetInstance().RemoveVehicle(id);
        }

        public IEnumerable<string> GetRates()
        {
            return Settings.Rate.Keys;
        }

        public IEnumerable<Vehicle> GetAllCars()
        {
            return Parking.GetInstance().ShowCars();
        }

        public IEnumerable<Vehicle> GetAllVehiclesOnParking()
        {
            return Parking.GetInstance().ShowStayedCars();
        }

        public string ReplenishBalance(int id, double balance)
        {
            return Parking.GetInstance().ReplenishBalance(id, balance);
        }
    }
}
