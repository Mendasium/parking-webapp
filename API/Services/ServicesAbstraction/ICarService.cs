﻿using API.Model;
using API.Model.DataTypesForControllers;
using API.Model.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Services.ServicesAbstraction
{
    public interface ICarService
    {
        Vehicle GetCarById(int id);
        string AddCar(ControllerCar vehicle);
        string RemoveCar(int id);
        IEnumerable<string> GetRates();
        IEnumerable<Vehicle> GetAllCars();
        IEnumerable<Vehicle> GetAllVehiclesOnParking();
        string ReplenishBalance(int id, double balance);
    }
}
