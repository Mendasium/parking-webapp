﻿using API.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Services.ServicesAbstraction
{
    public interface IParkingService
    {
        string PutToParking(int id);
        string RemoveFromParking(int id);
        IEnumerable<Transaction> GetLastTransactions();
        IEnumerable<Transaction> GetAllTransactions();
        IEnumerable<int> GetPlaces();
        string GetBalance();
    }
}
