﻿using API.Logger;
using API.Model;
using API.Model.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API
{
    public class Parking 
    {
        private static readonly Parking parking = new Parking();

        private Random rnd = new Random();
        private readonly List<Vehicle> registeredVehicles;
        private readonly List<Vehicle> removedVehicles;
        private readonly List<Transaction> lastTransations;
        private readonly List<StayedVehicle> stayedVehicles;
        private static PayCycle payCycle;

        private bool payStarted = false;

        public double Balance { get; private set; }

        private static ILogger<Transaction> logger;

        public Parking()
        {
            if(logger == null)
                logger = new EmptyLogger<Transaction>();
            registeredVehicles = new List<Vehicle>();
            lastTransations = new List<Transaction>();
            stayedVehicles = new List<StayedVehicle>();
            removedVehicles = new List<Vehicle>();
            Balance = Settings.PrimaryBalance;
        }

        public static Parking GetInstance() => parking;

        //Set loger and payCycle interface
        public void SetLoger(ILogger<Transaction> log)
        {
            logger = log;
        }
        public void SetPayCycle()
        {
            payCycle = new PayCycle(logger, registeredVehicles);
        }

        //Start Parking work
        public void StartWork()
        {
            if (!payStarted)
            {
                payCycle.Start(stayedVehicles, lastTransations);
                payStarted = true;
            }
        }

        public Vehicle GetCarById(int id)
        {
            return GetAuto(id);
        }

        //Add/Remove vehicle from parking DB
        public string AddVehicle(Vehicle auto)
        {
            auto.id = GenerateNewId();
            registeredVehicles.Add(auto);
            return $"Vehicle {auto} was added";
        }
        public string RemoveVehicle(int autoId)
        {
            Vehicle car = GetAuto(autoId);
            if(stayedVehicles.FirstOrDefault(x => x.vehicleId == autoId) != null)
            {
                return "Vehicle is needed to be picked up from the parking";
            }
            removedVehicles.Add(car);
            registeredVehicles.Remove(car);
            return $"vehicle {car} was deleted";
        }
            //Additional functions
        private int GenerateNewId()
        {
            int index = rnd.Next(1000);
            while (registeredVehicles.Find(v => v.id == index) != null)
                index = rnd.Next(1000);
            return index;
        }
        private Vehicle GetAuto(int autoId)
        {
            return registeredVehicles.FirstOrDefault(x => x.id == autoId);
        }


        //Show list of vehicles
        public IEnumerable<Vehicle> ShowCars()
        {
            if(registeredVehicles.Count == 0)
            {
                return new Vehicle[0];
            }
            return registeredVehicles;
        }
        public IEnumerable<Vehicle> ShowStayedCars()
        {
            if (stayedVehicles.Count == 0)
            {
                return new Vehicle[0];
            }
            return stayedVehicles.Select(v => registeredVehicles.FirstOrDefault(x => x.id == v.vehicleId));
        }


        //Show empty/busy places at the parking
        public int GetEmptyPlacesCount()
        {
            int stayedCount = stayedVehicles.Count;
            return Settings.VehicleMaxCount - stayedCount;
        }
        public int GetBusyPlacesCount()
        {
            return stayedVehicles.Count;
        }

        //Put/Remove vehicle to the parking
        public string PutToParking(StayedVehicle auto)
        {
            if(stayedVehicles.Find(v => v.vehicleId == auto.vehicleId) != null)
            {
                return "This vehicle is already in the parking lot";
            }

            stayedVehicles.Add(auto);
            return $"Vehicle {auto.vehicleId} was putted for parking at {DateTime.Now.TimeOfDay}";
        }
        public string RemoveFromParking(int autoId)
        {
            Vehicle car = GetAuto(autoId);
            StayedVehicle stayedCar = stayedVehicles.FirstOrDefault(v => v.vehicleId == autoId);
            if (stayedCar == null)
            {
                return "This vehicle isn't in the parking slot";
            }
            if(stayedCar.summ > car.Balance)
            {
                return $"Vehicle {stayedCar.vehicleId} doesn't have enough money to pay for parking. It needs {Math.Abs(car.Balance - stayedCar.summ)} more";
            }

            Transaction transaction = new Transaction(stayedCar.vehicleId, stayedCar.summ, DateTime.Now);
            lastTransations.Add(transaction);
            car.RemoveBalance(stayedCar.summ);
            Balance += stayedCar.summ;
            stayedVehicles.Remove(stayedCar);
            return $"Vehicle {car} was removed from parking at {DateTime.Now.TimeOfDay}, remaining balance - {car.Balance}";
        }

        //Summ of last minute earnings
        public double LastMinuteEarnings()
        {
            return lastTransations.Select(t => t.Summ).Sum();
        }

        //Show transactions which was made during the last minute
        public IEnumerable<Transaction> ShowTransactionByMinute()
        {
            if (lastTransations.Count == 0)
            {
                return new Transaction[0];
            }
            return lastTransations;
        }

        //Show all transactions during program lifetime
        public IEnumerable<Transaction> ShowAllTransation()
        {
            var allTransactions = logger.LoadData();
            if (allTransactions.Count() == 0 && lastTransations.Count == 0)
            {
                return new Transaction[0];
            }
            return Enumerable.Concat(allTransactions,  lastTransations);
        }

        //Replenish vehicle balance
        public string ReplenishBalance(int carId, double money)
        {
            Vehicle vehicle = GetAuto(carId);
            if (vehicle == null)
                return $"No auto with number {carId} at the parking";
            vehicle.AddBalance(money);
            return $"Balance of {carId} balance was replenished for {money}";
        }
    }
}

//+Узнать текущий баланс Парковки.
//+Сумму заработанных денег за последнюю минуту.
//+Узнать количество свободных/занятых мест на парковке.
//+Вывести на экран все Транзакции Парковки за последнюю минуту
//+Вывести всю историю Транзакций (считав данные из файла Transactions.log)
//+Вывести на экран список всех Транспортных средств
//+Создать/поставить Транспортное средство на Парковку
//+Удалить/забрать Транспортное средство с Парковки
//+Пополнить баланс конкретного Транспортного средства
