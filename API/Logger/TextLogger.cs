﻿using API.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace API.Logger
{
    public class TransactionLogger : ILogger<Transaction>
    {
        public static string path = Environment.CurrentDirectory;
        public IEnumerable<Transaction> LoadData()
        {
            if (!File.Exists(path + "/Transactions.log"))
                return new List<Transaction>();
            List<Transaction> transactions = new List<Transaction>();
            using (StreamReader sr = new StreamReader(path + "/Transactions.log"))
            {
                string line = sr.ReadLine();
                while (line != null)
                {
                    int currentPos = 0;
                    int vehicleId = int.Parse(line.Substring(0, line.IndexOf('$', currentPos)));
                    currentPos = line.IndexOf('$', currentPos) + 1;
                    double summ = double.Parse(line.Substring(currentPos, line.IndexOf('$', currentPos) - currentPos));
                    currentPos = line.IndexOf('$', currentPos) + 1;
                    DateTime time = DateTime.Parse(line.Substring(currentPos, line.Length - currentPos));
                    transactions.Add(new Transaction(vehicleId, summ, time));
                    line = sr.ReadLine();
                }
            }
            return transactions;
        }

        public void WriteData(IEnumerable<Transaction> data)
        {
            if (!File.Exists(path + "/Transactions.log"))
                File.Create(path + "/Transactions.log");
            using (StreamWriter write = new StreamWriter(path + "/Transactions.log", true))
            {
                foreach(Transaction transaction in data)
                {
                    string resultLime = transaction.VehicleId + "$" + transaction.Summ + "$" + transaction.Time.ToString();
                    write.WriteLine(resultLime);
                }
            }
        }
    }
}
