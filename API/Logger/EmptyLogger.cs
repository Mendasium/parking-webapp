﻿using API.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Logger
{
    public class EmptyLogger<T> : ILogger<T>
    {
        public IEnumerable<T> LoadData()
        {
            throw new NotImplementedException("Add logger before its using");
        }

        public void WriteData(IEnumerable<T> data)
        {
            throw new NotImplementedException("Add logger before its using");
        }
    }
}
