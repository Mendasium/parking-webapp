﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Logger
{
    public interface ILogger<T>
    {
        void WriteData(IEnumerable<T> data);
        IEnumerable<T> LoadData();
    }
}
