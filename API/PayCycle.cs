﻿
using API.Logger;
using API.Model;
using API.Model.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace API
{
    public class PayCycle
    {
        private readonly int timeToSave = Settings.SecondsToSave;
        private readonly double timeToPay = Settings.SecondsToPay;
        private readonly double fine = Settings.Fine;
        Timer ParkingPay;
        Timer Saver;

        ILogger<Transaction> logger;
        IEnumerable<Vehicle> vehicles;

        public PayCycle(ILogger<Transaction> logger, IEnumerable<Vehicle> vehicles)
        {
            this.logger = logger;
            this.vehicles = vehicles;
        } 

        void Save(object data)
        {
            List<Transaction> transactions = (List<Transaction>)data;
            logger.WriteData(transactions);
            transactions.Clear();
        }


        void PayForParking(object data)
        {
            IEnumerable<StayedVehicle> stayedVehicles = (IEnumerable<StayedVehicle>)data;
            foreach(StayedVehicle stayedVehicle in stayedVehicles)
            {
                Vehicle vehicle = vehicles.FirstOrDefault(v => v.id == stayedVehicle.vehicleId);
                if (vehicle == null)
                    throw new Exception("There is unregistered vehicle at the parking");
                double secondsPaying = vehicle.Rate;

                if(!stayedVehicle.FineStarted && stayedVehicle.summ + secondsPaying > vehicle.Balance)
                    stayedVehicle.StartFine();

                if (stayedVehicle.FineStarted)
                    stayedVehicle.summ += secondsPaying * fine;
                else
                    stayedVehicle.summ += secondsPaying;
            }
        }

        public void Start(IEnumerable<StayedVehicle> stayedVehicles, List<Transaction> transactions)
        {
            TimerCallback parkingTimer = new TimerCallback(PayForParking);
            TimerCallback saveTimer = new TimerCallback(Save);

            ParkingPay = new Timer(parkingTimer, stayedVehicles, (int)(timeToPay * 1000), (int)(timeToPay * 1000));
            Saver = new Timer(saveTimer, transactions, timeToSave * 1000, timeToSave * 1000);
        }
    }
}
