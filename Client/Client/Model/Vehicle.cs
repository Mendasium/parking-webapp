﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Client.Model
{
    public class Vehicle
    {
        public int id;
        public double Balance { get; set; }
        public double Rate { get; }
        public string Type { get; set; }

        public override string ToString()
        {
            return id.ToString();
        }
    }
}
