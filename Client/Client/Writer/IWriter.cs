﻿using Client.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Writer
{
    public interface IWriter
    {
        void ShowTransactions(IEnumerable<Transaction> transactions);
        void ShowAutos(IEnumerable<Vehicle> vehicles);
        void WriteException(string exeption);
        void Write(string message, bool showLine = true);
        void WriteInfo(string message, bool showLine = false);
        void WriteMenu(IEnumerable<string> menu);
    }
}
