﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.Model;

namespace Client.Writer
{
    public class ConsoleWriter : IWriter
    {
        public void ShowAutos(IEnumerable<Vehicle> vehicles)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.Write(new string('-', Console.WindowWidth));
            foreach (Vehicle vehicle in vehicles)
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write("\tVehicle Number:\t\t\t");
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write("|\t");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine(vehicle.id);

                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write("\tVehicle type:\t\t\t");
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write("|\t");
                Console.ForegroundColor = ConsoleColor.Yellow;
                string vehicleType = vehicle.GetType().ToString();
                Console.WriteLine(vehicleType.Substring(vehicleType.LastIndexOf('.') + 1));
             
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write("\tBalance:\t\t\t");
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write("|\t");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine(vehicle.Balance);

                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write(new string('-', Console.WindowWidth));
            }
            Console.WriteLine();
            Console.ResetColor();
        }

        public void ShowTransactions(IEnumerable<Transaction> transactions)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.Write(new string('-', Console.WindowWidth));
            foreach (Transaction transaction in transactions)
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write("\tVehicle number:\t\t");
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write("|\t");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine(transaction.VehicleId);

                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write("\t\tPaid sum:\t");
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write("|\t");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine(transaction.Summ);

                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write("\tRegistration time:\t");
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write("|\t");
                Console.ForegroundColor = ConsoleColor.Yellow;
                DateTime time = transaction.Time;
                Console.WriteLine(time.Hour + " : " + time.Minute + " : " + time.Second);

                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write(new string('-', Console.WindowWidth));
            }
            Console.ResetColor();
        }

        public void Write(string message, bool showLine = true)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(message);
            if (showLine)
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write(new string('-', Console.WindowWidth));
            }
            Console.ResetColor();
        }

        public void WriteException(string exception)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(exception);
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine(new string('-', Console.WindowWidth));
            Console.ResetColor();
            Console.ResetColor();
        }

        public void WriteInfo(string message, bool showLine = false)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(message);
            if(showLine)
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write(new string('-', Console.WindowWidth));
            }
            Console.ResetColor();
        }

        public void WriteMenu(IEnumerable<string> menu)
        {
            bool even = false;
            foreach(string menuItem in menu)
            {
                Console.ForegroundColor = even ? ConsoleColor.Magenta : ConsoleColor.DarkMagenta;
                Console.WriteLine("\t" + menuItem);
                even = !even;
            }
            Console.ResetColor();
        }
    }
}
