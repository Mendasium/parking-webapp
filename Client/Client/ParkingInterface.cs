﻿using Client.Model;
using Client.Services;
using Client.Writer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class ParkingInterface
    {
        IWriter writer;
        private List<string> menu;
        private List<string> types;
        CarService _carService;
        ParkingService _parkingService;

        bool APIDoesntAnswer = false;

        public ParkingInterface()
        {
            writer = new ConsoleWriter();
            _carService = new CarService(new ConsoleWriter());
            _parkingService = new ParkingService(new ConsoleWriter());
            
            menu = new List<string>
            {
                " 1 - Add vehicle to parking DB",
                " 2 - Remove vehicle from parking DB",
                " 3 - Put vehicle on parking",
                " 4 - Remove vehicle from parking",
                " 5 - Replenish vehicle balance",
                " 6 - Show all cars on parking",
                " 7 - Show all cars",
                " 8 - Show all transactions, which was made during this minute",
                " 9 - Show all transactions",
                "10 - Show places count",
                "11 - Show parking balance"
            };

            types = new List<string>();
            types.AddRange(_carService.GetRates().Result);
            if (types.Count == 0)
            {
                APIDoesntAnswer = true;
                writer.WriteInfo("\t\t\t\tTry to start your API or change its URL in Settings.cs");
            }
        }

        public void Start()
        {
            if (!APIDoesntAnswer)
            {   
                StartWork();
                int result = 0;
                while (true)
                {
                    result = GetMenuItem();
                    if (result == 0)
                        continue;
                    if (result == 13)
                        break;
                    Show(result);
                }
            }
            FinishWork();
        }

        private void StartWork()
        {
            writer.WriteInfo("Welcomes you in parking programm \"My stopping\":)");
            writer.WriteInfo("So, let's start. \nTo open parking menu you can always enter \"MENU\".");
            writer.WriteInfo("To close application enter \"EXIT\"", true);
        }

        private void FinishWork()
        {
            writer.Write("\t\t\t\t\tThank you for using this application:)");
            writer.WriteInfo("\t\t\t\t\t      Click on Enter to exit");
            writer.Write("\t\t\t\t\t\tHave a nice day:)");
            Console.ReadLine();
        }

        private string CurrentTime()
        {
            DateTime dateTime = DateTime.Now;
            return dateTime.Hour + ":" + string.Format("{0:d2}", dateTime.Minute) + ":" + string.Format("{0:d2}", dateTime.Second);
        }

        private int GetMenuItem()
        {
            writer.Write("\t\t\t\t\t\t\tMenu\t\t\t\t\t\t\t" + CurrentTime());
            writer.WriteMenu(menu);
            string result = "";
            int res = -1;
            while (res < 0)
            {
                result = Console.ReadLine();
                if (result == "MENU")
                    return 0;
                if (result == "EXIT")
                    return 13;
                if (!int.TryParse(result, out res) || (res > 12 || res <= 0))
                {
                    res = -1;
                    writer.WriteInfo("Please, enter correct number of menu item");
                }
            }
            return res;
        }

        private void AddAuto()
        {
            writer.Write("Enter car type", false);
            writer.WriteMenu(Enumerable.Range(1, types.Count).Select(i => i + " " + types[i - 1]));
            Vehicle vehicle = null;
            while (vehicle == null)
            {
                string result = Console.ReadLine();
                if (!int.TryParse(result, out int res) || (res > types.Count || res <= 0))
                {
                    writer.WriteException("Enter correct type, please");
                    continue;
                }
                vehicle = new Vehicle{
                    Type = types[res - 1]
                };
            }
            writer.Write("Enter car balance");
            while (vehicle.Balance == 0)
            {
                string result = Console.ReadLine();
                if (!double.TryParse(result, out double res) || res < 0)
                {
                    writer.WriteException("Enter correct balance, please");
                    continue;
                }
                vehicle.Balance = res;
            }
            writer.WriteInfo(_carService.AddCar(vehicle).Result);
        }

        private void RemoveVehicle()
        {
            int carNumber = GetVehicleNumber();
            writer.WriteInfo(_carService.RemoveCar(carNumber).Result);
        }

        private void ReplenishVehicleBalance()
        {
            int carNumber = GetVehicleNumber();
            writer.Write("Enter car balance for replenishment");
            double balance = 0;
            while (balance == 0)
            {
                string result = Console.ReadLine();
                if (!double.TryParse(result, out double res) || res < 0)
                {
                    writer.WriteException("Enter Correct Balance");
                    continue;
                }
                balance = res;
            }
            writer.WriteInfo(_carService.ReplenishBalance(carNumber, balance).Result);
        }

        private void SetVehicleToParking()
        {
            int carNumber = GetVehicleNumber();
            writer.WriteInfo(_parkingService.SetCarToParking(carNumber).Result);
        }

        private void RemoveVehicleFromParking()
        {
            int carNumber = GetVehicleNumber();
            writer.WriteInfo(_parkingService.RemoveCarFromParking(carNumber).Result);
        }

        private int GetVehicleNumber()
        {
            writer.Write("Enter vehicle number", false);
            int carNumber = -1;
            while (carNumber == -1)
            {
                string result = Console.ReadLine();
                if (!int.TryParse(result, out int res) || res <= 0)
                {
                    writer.WriteException("Please, enter correct number");
                    continue;
                }
                carNumber = res;
            }
            return carNumber;
        }

        private void Show(int menuitem)
        {
            switch (menuitem)
            {
                case 1:
                    AddAuto();
                    break;
                case 2:
                    RemoveVehicle();
                    break;
                case 3:
                    SetVehicleToParking();
                    break;
                case 4:
                    RemoveVehicleFromParking();
                    break;
                case 5:
                    ReplenishVehicleBalance();
                    break;
                case 6:
                    writer.Write("Vehicles on parking:", false);
                    writer.ShowAutos(_carService.ShowVehiclesOnParking().Result);
                    break;
                case 7:
                    writer.Write("All vehicles:", false);
                    writer.ShowAutos(_carService.ShowAllVehicles().Result);
                    break;
                case 8:
                    writer.Write("Transactions during last minute:", false);
                    writer.ShowTransactions(_parkingService.GetLastTransactions().Result);
                    break;
                case 9:
                    writer.Write("All transactions:", false);
                    writer.ShowTransactions(_parkingService.GetAllTransactions().Result);
                    break;
                case 10:
                    int[] placeNumbers = _parkingService.GetPlacesCount().Result.ToArray();
                    writer.Write("Places at parking count - " + placeNumbers[0], false);
                    writer.Write("Empty places count - " + placeNumbers[1], false);
                    writer.Write("Busy places count - " + placeNumbers[2]);
                    break;
                case 11:
                    writer.Write("Parking balance - " + _parkingService.GetBalance().Result);
                    break;
                default:
                    writer.WriteException("Choose correct menu item");
                    break;
            }
        }
    }
}
