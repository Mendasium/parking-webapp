﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            ParkingInterface parkingInterface = new ParkingInterface();
            parkingInterface.Start();
        }
    }
}
