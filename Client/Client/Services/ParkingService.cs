﻿using Client.Model;
using Client.Writer;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Client.Services
{
    public class ParkingService
    {
        private static HttpClient Client = new HttpClient();
        private readonly string httpError = Settings.HttpNoAnswer;

        readonly Uri Url;
        readonly IWriter writer;

        public ParkingService(IWriter writer)
        {
            Url = new Uri(Settings.GlobalURL);
            this.writer = writer;
        }

        private async Task<string> HttpPut(string url, int id)
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Put, url + id);
                HttpResponseMessage response = await Client.SendAsync(request);
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return responseBody;
            }
            catch (HttpRequestException e)
            {
                writer.WriteException($"{httpError} {e.Message}");
                return "";
            }
        }
        private async Task<IEnumerable<Transaction>> HttpGetTransactions(string url)
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url);
                HttpResponseMessage response = await Client.SendAsync(request);
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<Transaction>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                writer.WriteException($"{httpError} {e.Message}");
                return new Transaction[0];
            }
        }

        public async Task<string> SetCarToParking(int id)
        {
            return await HttpPut(Url + "parking/putToParking/", id);
        }

        public async Task<string> RemoveCarFromParking(int id)
        {
            return await HttpPut(Url + "parking/removeFromParking/", id);
        }

        public async Task<IEnumerable<Transaction>> GetAllTransactions()
        {
            return await HttpGetTransactions(Url + "parking/allTransactions/");
        }

        public async Task<IEnumerable<Transaction>> GetLastTransactions()
        {
            return await HttpGetTransactions(Url + "parking/lastTransactions/");
        }

        public async Task<string> GetBalance()
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, Url + "parking/getBalance");
                HttpResponseMessage response = await Client.SendAsync(request);
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return responseBody;
            }
            catch (HttpRequestException e)
            {
                writer.WriteException($"{httpError} {e.Message}");
                return "";
            }
        }

        public async Task<IEnumerable<int>> GetPlacesCount()
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, Url + "parking/places");
                HttpResponseMessage response = await Client.SendAsync(request);
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<int>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                writer.WriteException($"{httpError} {e.Message}");
                return new int[3];
            }
        }
    }
}
