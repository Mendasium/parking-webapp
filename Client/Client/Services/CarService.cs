﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Client.Model;
using Client.Writer;
using Newtonsoft.Json;

namespace Client.Services
{
    //+Сохранение транзакций было сделать через таймеры, а не треды.
    //+Лучше текст в коде писать на англ., чтобы не было проблем с кодировкой.
    //+Строки лучше форматировать используя $"".
    //+Логи лучше хранить в строчном виде, а не json, чтобы не было проблем с добавлением в конец.

    //+Старт парковки должен быть реализован в классе Startup
    //+Можно использовать HttpClient для отправки запросов с клиента
    //+Используйте JSON как формат данных для общения между приложениями
    //+Старайтесь не усложнять(не нужно подключать базу данных, заморачиваться с асинхронностью и т.д.)
    public class CarService
    {
        private static HttpClient Client = new HttpClient();
        private readonly string httpError = Settings.HttpNoAnswer;

        readonly Uri Url;
        readonly IWriter writer;

        public CarService(IWriter writer)
        {
            Url = new Uri(Settings.GlobalURL);
            this.writer = writer;
        }

        private async Task<string> HttpGet(string url)
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url);
                HttpResponseMessage response = await Client.SendAsync(request);
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return responseBody;
            }
            catch (HttpRequestException e)
            {
                writer.WriteException($"{httpError} {e.Message}");
                return "";
            }
        }
        private async Task<string> HttpDelete(string url)
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Delete, url);
                HttpResponseMessage response = await Client.SendAsync(request);
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return responseBody;
            }
            catch (HttpRequestException e)
            {
                writer.WriteException($"{httpError} {e.Message}");
                return "";
            }
        }
        private async Task<string> HttpPostAdd(string url, object data)
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, url);
                var content = JsonConvert.SerializeObject(data);
                request.Content = new StringContent(content, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await Client.SendAsync(request);
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return responseBody;
            }
            catch (HttpRequestException e)
            {
                writer.WriteException($"{httpError} {e.Message}");
                return "";
            }
        }
        
        public async Task<IEnumerable<string>> GetRates()
        {
            try
            {
                HttpResponseMessage response = await Client.GetAsync(Url + "car/rates");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                var rates = JsonConvert.DeserializeObject<IEnumerable<string>>(responseBody);
                return rates;
            }
            catch (HttpRequestException e)
            {
                writer.WriteException($"{httpError} {e.Message}");
                return new string[0];
            }
        }

        public async Task<string> AddCar(Vehicle vehicle)
        {
            return await HttpPostAdd(Url + "car/add", vehicle);
        }

        public async Task<string> RemoveCar(int id)
        {
            return await HttpDelete(Url + "car/remove/" + id);
        }

        public async Task<IEnumerable<Vehicle>> ShowVehiclesOnParking()
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, Url + "car/getOnParking");
                HttpResponseMessage response = await Client.SendAsync(request);
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<Vehicle>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                writer.WriteException($"{httpError} {e.Message}");
                return new Vehicle[0];
            }
        }

        public async Task<IEnumerable<Vehicle>> ShowAllVehicles()
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, Url + "car/getAll");
                HttpResponseMessage response = await Client.SendAsync(request);
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<Vehicle>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                writer.WriteException($"{httpError} {e.Message}");
                return new Vehicle[0];
            }
        }

        public async Task<string> ReplenishBalance(int carId, double balance)
        {
            return await HttpPostAdd(Url + "car/replenishBalance", new { CarId = carId, Balance = balance });
        }
    }
}
